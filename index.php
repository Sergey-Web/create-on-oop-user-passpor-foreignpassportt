<?php 
error_reporting(-1);
ini_set('display_errors',1);
header('Content-Type: text/html; charset=utf-8');

class User
{
	protected $userData = NULL;

	public function __construct($login = NULL, $email = NULL)
	{
		if($login === NULL || $email === NULL) {
			return fasle;
		}

		$userData = ['login'=>$login, 'email'=>$email];

		$this->userData = $userData;
	}

	public function operation()
	{
		return $this->userData;
	}

}

abstract class DecoratorUser extends User
{
	public function __construct(User $userData)
	{
		$this->userData = $userData;; 
	}

	protected function getUserDate()
	{
		return $this->userData;
	}

	public function operation()
	{
		return $this->getUserDate()->operation();
	}
}

class UserValidation extends DecoratorUser 
{
	public function operation()
	{
		if(!filter_var(parent::operation()['email'], FILTER_VALIDATE_EMAIL)) {
			return 'Error login or email';
		}
		return parent::operation();
	}
}

class Passport
{
	protected $passport = NULL;

	public function __construct($name, $surname, $patronymic, $birthDate, $placeReg)
	{
		try {
			if($name === NULL || $surname === NULL || $patronymic === NULL ||
				$birthDate === NULL || $placeReg === NULL) {
				throw new Exception('Error data passport!');
			}
		} catch(Exception $e) {
			return $e->getMessage();
		}

		$passport = [
					 'name'       => $name,
					 'surname'    => $surname,
					 'patronymic' => $patronymic,
				 	 'birthDate'  => $birthDate,
				 	 'placeReg'   => $placeReg
				 	];

	 	$this->passport = $passport;
	}

	public function pullData()
	{
		return $this->passport;
	}
}

class ForeignPassport extends Passport
{
	private $foreignPassport = NULL;

	public function __construct($info, $validity)
	{
		try {
			if($info === NULL || $validity === NULL) {
				throw new Exception('Error foreign passport!');
			}
		} catch(Exception $e) {
			return $e->getMessage();
		}

		$userData = ['info'=>$info, 'validity'=>$validity];
		$this->foreignPassport[] = $info;
		$this->isValid($validity);
	}

	public function isValid($validity)
	{
		$inspection = new ValidDate($validity);

		return $this->foreignPassport[] = $inspection->validDate;;
	}

	public function pullData()
	{
		return $this->foreignPassport;
	}
}

class ValidDate
{
	public $validDate;

	function __construct($validity)
	{
		$dateParse = strtotime($validity);
		
		if($dateParse > time()) {
			$this->validDate = 1;
			return $this->validDate;
		} else {
			$this->validDate = 0;
			return $this->validDate;
		}
	}
}

class DB
{
	static $instance = NULL;

	private function __construct() {}
	private function __clone() {}
	private function __wakeup() {}

	static function getInstance()
	{
		if(self::$instance !== NULL) {
			return self::$instance;
		}
		try {
			$link = new PDO('mysql:host=localhost;dbname=test; 
				charset=utf8', 'root', 'qwerty');
			$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {
			return $e->getMessage();	
		}
		self::$instance = $link;

		return self::$instance;

	}

}


class Routing
{
	private $user = NULL;
	private $passport = NULL;
	static $foreignPassport = NULL;

	public function __set($key, $arr)
	{
		if($key == 'foreignPassport') {
			$foreignPassport = new ForeignPassport($arr[0],$arr[1]);
			$arr = $foreignPassport->pullData();
			self::$foreignPassport[] = ['info'=>$arr[0],'validity'=>$arr[1]];
			return false;
		}
	}

	public function user($name, $email)
	{
		$user = new UserValidation(new User($name, $email));

		return $this->user = $user->operation();
	}

	public function passport($name, $surname, $patronymic, $birthDate, $placeReg)
	{
		$passport = new Passport($name, $surname, $patronymic, $birthDate, $placeReg);

		return $this->passport = $passport->pullData();
	}

	private function query($sql, $val)
	{
		if(!is_array($val)) {
			return 'The second value should be an array';
		}

		$link = DB::getInstance();
		$result = $link->prepare($sql);
		return $result->execute($val);
	}

	public function connect()
	{
		$user = $this->user;
		$passport = $this->passport;
		$arrayForeignPassport = self::$foreignPassport;

		if($user === NULL) {

			return 'Error data user';

		} elseif($passport === NULL) {

			return 'Error data passport';

		} elseif(count($arrayForeignPassport) == 0) {

			$sqlUser = "INSERT INTO users (login,email) VALUES (:login,:email)";
			$queryUser = $this->query($sqlUser, $user);

			$passport = array_merge(['idUser'=>DB::$instance->lastInsertId()], $passport);

			$sqlPassport = "INSERT INTO passports (id_user,name,surname,patronymic,birth_date,place_reg) 
				VALUES (:idUser,:name,:surname,:patronymic,:birthDate,:placeReg)";
			$queryPassport = $this->query($sqlPassport, $passport);

		} else {

			$sqlUser = "INSERT INTO users (login,email) VALUES (:login,:email)";
			$queryUser = $this->query($sqlUser, $user);

			$passport = array_merge(['idUser'=>DB::$instance->lastInsertId()], $passport);

			$sqlPassport = "INSERT INTO passports (id_user,name,surname,patronymic,birth_date,place_reg) 
				VALUES (:idUser,:name,:surname,:patronymic,:birthDate,:placeReg)";
			$queryPassport = $this->query($sqlPassport, $passport);

			foreach ($arrayForeignPassport as $foreignPassport) {
				$foreignPassport = array_merge($passport, $foreignPassport);
				$sqlForeignPassport = "INSERT INTO foreign_passports (id_user,name,surname,patronymic,birth_date,place_reg,info,validity) 
					VALUES (:idUser,:name,:surname,:patronymic,:birthDate,:placeReg,:info,:validity)";
				$queryForeignPassport = $this->query($sqlForeignPassport, $foreignPassport);
			}


		}

	}

}

/*$routing = new Routing();
$user = $routing->user('login', 'email@mail.ru');
$passport = $routing->passport('name', 'surname', 'patronymic', '26.10.1955', 'placeReg');
$routing->foreignPassport = ['info','25.10.16'];
$routing->foreignPassport = ['info','26.11.16'];
$routing->connect();*/
